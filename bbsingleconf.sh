#!/bin/bash
#
# Per Kristian Lehre
# P.K.Lehre@cs.bham.ac.uk
# January 2022
#
# Launch jobs on the bluebear cluster directly from local machine
# using configuration file.
#
# E.g.,
#
# bbsingleconf.sh conf-file
#

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "Usage:"
    echo "$0 config-file experiment-name"
    exit 1
fi

conf_lines_file=$(mktemp /tmp/bbsingleconf.XXXXXX)
app_singleconf --dryrun $1 > $conf_lines_file
bbconffile.sh $conf_lines_file $2

