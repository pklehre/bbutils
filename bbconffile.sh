#!/bin/bash
#
# Per Kristian Lehre
# P.K.Lehre@cs.bham.ac.uk
# January 2018
#
# Launch jobs on the bluebear cluster directly from local machine
# taking configurations from a config file with one line per configuration.

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "Usage:"
    echo "$0 config-file experiment-name"
    exit 1
fi

if [[ -z "$bbusername" ]]; then
    echo "Set environment variable bbusername to your username on bluebear"
    exit 1
else
    username=$bbusername
fi

if [[ -z "$bbhost" ]]; then
    echo "Set environment variable bbhost to host name of bluebear, e.g. export bbhost=bluebear.bham.ac.uk"
    exit 1
else
    cluster=$bbhost
fi

if [[ -z "bbpath" ]]; then
    echo "Set environment variable bbpath to path of experiments, e.g. /rds/homes/l/lehrepk/bbexp"
    exit 1
else
    cluster_path=$bbpath
fi

if [[ -z "bbbinpath" ]]; then
    echo "Set environment variable bbbinpath to path singularity image, e.g. /rds/homes/l/lehrepk/bbexp"
    exit 1
else
    bin_path=$bbbinpath
fi

if [[ -z "bbsingularityurl" ]]; then
    echo "Set environment variable bbsingularityurl to url of singularity image, e.g. library://pklehre/default/app_algorithm2:1.0"
    exit 1
else
    singularity_url=$bbsingularityurl
fi

if [[ -z "bbsingularityimage" ]]; then
    echo "Set environment variable bbsingularity_image to singularity image, e.g. app_algorithm2_1.0.sif"
    exit 1
else
    singularity_image=$bbsingularityimage
fi



# username=lehrepk
# cluster=bluebear.bham.ac.uk
# cluster_path=/rds/homes/l/lehrepk/bbexp #/rds/projects/2018/lehrepk-convergence/bbexp
# bin_path=/rds/homes/l/lehrepk/bbexp
# singularity_url="library://pklehre/default/app_algorithm2:1.0"
# singularity_image="app_algorithm2_1.0.sif"

# Get the command and its arguments
echo "Getting the command and its arguments"
conffile=$1
conffile_basename=$(basename $conffile)

# Create a directory for the job
echo "Creating directory for the job"
jobid="slurm_bbconffile_$2_"$(date +%F-%H-%M-%S)
mkdir /tmp/$jobid
mkdir /tmp/$jobid/results
cp $conffile /tmp/$jobid



#cp $cmdpath /tmp/$jobid

numjobs=$(cat $conffile | wc -l)
let lastindex=$numjobs-1


# Write slurm job submission script
echo "Creating script file"
scriptfile="$jobid/script.sh"


cat <<EOF > /tmp/$scriptfile
#!/bin/bash
#
# Script created by bbconffile.sh
#
EOF
echo "# $@" >> /tmp/$scriptfile

cat <<EOF >> /tmp/$scriptfile
#
# 
#SBATCH --mem=10000
#SBATCH --time=96:00:00
#SBATCH -o results/job_%A.out
#SBATCH -e results/job_%A.err
#SBATCH --open-mode=append
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV

module purge
module load bluebear
#module add glibc/2.28-GCCcore-6.4.0
#module load gsl/1.9-10.3-foss-2018a-R-3.5.0
app_fileconf_argument="experiment=\\"fileconf\\"; file=\\"$conffile_basename\\"; line=\$SLURM_ARRAY_TASK_ID;"
singularity exec /rds/homes/l/lehrepk/bbexp/app_algorithm2_1.0.sif /app_fileconf \$app_fileconf_argument
#
#
EOF

# Copy the directory to the cluster and launch the job
echo "Copying the directory to the cluster"
# We first create the directory on the remote side if it does not already exist as
# a workaround to the bug in RedHat provided sshd described here:
# https://groups.google.com/g/opensshunixdev/c/tCbaeKbABvM/m/rUE7mkrnFAAJ

echo $cluster_path/$jobid
ssh $username@$cluster mkdir -p $cluster_path/$jobid
scp -r /tmp/$jobid $username@$cluster:$cluster_path
ssh -T $username@$cluster <<EOF 
    bash
    #opam update
    #opam upgrade
    # cd eclib
    # git pull
    # make
    # make install
    cd $bin_path
    singularity pull --force --disable-cache $singularity_url
    cd $cluster_path/$jobid 
    sbatch --array=0-$lastindex script.sh
    watch squeue
EOF
ssh -t $username@$cluster watch squeue
#ssh lehrepk@bluebear.bham.ac.uk "echo $jobid ; echo 1-$numjobs script.sh ; sleep 5"








