#!/bin/bash
#
# Per Kristian Lehre
# P.K.Lehre@cs.bham.ac.uk
# January 2018
#
# Launch jobs on the baskerville cluster directly from local machine.
#
# E.g.,
#
# bvconffile.sh jobs.txt
#

username=lehrepk
cluster=login.baskerville.ac.uk
cluster_path=/bask/homes/l/lehrepk/bvconf #/rds/projects/2018/lehrepk-convergence/bbexp
#bin_path=/rds/homes/l/lehrepk/eclib/_build/default/bin
bin_path=/bask/homes/l/lehrepk/bvconf

# Get the command and its arguments
echo "Getting the command and its arguments"
conffile=$1
conffile_basename=$(basename $conffile)

# Create a directory for the job
echo "Creating directory for the job"
jobid="slurm_bvconffile_"$(date +%F-%H-%M-%S)
mkdir /tmp/$jobid
mkdir /tmp/$jobid/results
cp $conffile /tmp/$jobid
#cp $cmdpath /tmp/$jobid

numjobs=$(cat $conffile | wc -l)
let lastindex=$numjobs-1


# Write slurm job submission script
echo "Creating script file"
scriptfile="$jobid/script.sh"

cat <<EOF > /tmp/$scriptfile
#!/bin/bash
#
# Script created by bvconffile.sh
#
EOF
echo "# $@" >> /tmp/$scriptfile

cat <<EOF >> /tmp/$scriptfile
#
# 
#SBATCH --mem=10000
#SBATCH --time=24:00:00
#SBATCH --account=lehrepk-cudaec
#SBATCH --qos=bham
#SBATCH -o results/job_%A_%a.out
#SBATCH -e results/job_%A_%a.err
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV


#module purge
#module load bluebear
#module add glibc/2.28-GCCcore-6.4.0
#module load gsl/1.9-10.3-foss-2018a-R-3.5.0

#singularity pull --arch amd64 library://pklehre/default/app_algorithm2:1.0
../app_algorithm2_1.0.sif --file=\"$conffile_basename\" --line=\$SLURM_ARRAY_TASK_ID 
#
#
EOF

# Copy the directory to the cluster and launch the job
echo "Copying the directory to the cluster"
scp -r /tmp/$jobid $username@$cluster:$cluster_path
ssh -T $username@$cluster <<EOF
    bash
    cd $cluster_path/$jobid 
    pwd
    sbatch --array=0-$lastindex script.sh
EOF

#sbatch --array=0-$lastindex $cluster_path/$jobid/script.sh
#ssh -t $username@$cluster watch squeue
#ssh lehrepk@bluebear.bham.ac.uk "echo $jobid ; echo 1-$numjobs script.sh ; sleep 5"








