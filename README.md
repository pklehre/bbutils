# README #

Repository for routines to control jobs submited to the bluebear cluster.
Currently, it only contains the script bblaunch.sh. 

### Requirements ###

* Gnu Parallel

### Example usage ###

In the following example, we will run the executable selfadapt with 
different parameter settings for id, chi, and problem size n.
It is assumed that the executable is compiled for the same hardware
architecture (glibc version etc) as on bluebear.

The script creates a slurm script which is copied to bluebear together
with the executable, and the job is launched on bluebear.

The syntax for specifying parameters follows that of Gnu Parallel.

~~~~
bblaunch.sh selfadapt -id {1} -chi {2} -n {3} ::: $(seq 1 100) ::: 0.1 0.2 ::: 100 200
~~~~

When parameters need to be replaced within a double quoted parameter (e.g.,
pcnfratio within the params option in the example below) enclose
double quotes with single quotes on the command line. 


~~~~

bblaunch.sh app_sgaonemax2 --fitness plantedcnf --algorithm psva --params '"n=1000; pcnfratio={1}; lambda=n; chi=0.68; k=2; max_t=10000"' --selection tournament --statistics pcnf --repetitions 10 ::: 1.0 2.0

~~~~
