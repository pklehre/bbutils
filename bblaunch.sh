#!/bin/bash
#
# Per Kristian Lehre
# P.K.Lehre@cs.bham.ac.uk
# January 2018
#
# Launch jobs on the bluebear cluster directly from local machine
# using parallel syntax. The executable is copied to the cluster
# and a sbatch script is created and launched.
#
#
# E.g.,
#
# bblaunch.sh ./app_test.exe -chi 0.5 -n {1} -repetitions 1 ::: 100 200
#

username=lehrepk
cluster=bluebear.bham.ac.uk
cluster_path=/rds/homes/l/lehrepk/bbexp #/rds/projects/2018/lehrepk-convergence/bbexp
#bin_path=/rds/homes/l/lehrepk/eclib/_build/default/bin
bin_path=/rds/homes/l/lehrepk

# Get the command and its arguments
echo "Getting the command and its arguments"
cmdpath=$1
#cmd=$bin_path"/"$(basename $1)".exe"
cmd=$(basename $1)
rem=""
shift
for i in "$@"; do
    rem="$rem$i "
done

#echo "cmd=$cmd"
#exit

# Create a directory for the job
echo "Creating directory for the job"
jobid="slurm_"$cmd"_"$(date +%F-%H-%M-%S)
mkdir /tmp/$jobid
mkdir /tmp/$jobid/results
#cp $cmdpath /tmp/$jobid

# Create a list of jobs to run on the cluster

function jobs {
    # parallel -q  --shuf echo $bin_path"/"$cmd".exe" $rem
    parallel -q  --shuf echo $cmd $rem
}

function nth {
    N=$1
    sed -n "${N}p"
}


jobsfile=/tmp/$jobid/jobs.txt
jobs > $jobsfile
numjobs=$(cat $jobsfile | wc -l)


# Write slurm job submission script
echo "Creating script file"
scriptfile="$jobid/script.sh"

cat <<EOF > /tmp/$scriptfile
#!/bin/bash
#
# Script created by bblaunch.sh
#
EOF
echo "# $@" >> /tmp/$scriptfile

cat <<EOF >> /tmp/$scriptfile
#
# 
#SBATCH --mem=10000
#SBATCH --time=96:00:00
#SBATCH -n 1         # Number of cores
#SBATCH -o results/job_%A_%a.out
#SBATCH -e results/job_%A_%a.err
#SBATCH --get-user-env
#SBATCH --mail-type=END
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV

module purge
module load bluebear
#module add glibc/2.28-GCCcore-6.4.0
#module load gsl/1.9-10.3-foss-2018a-R-3.5.0

#
#
EOF


for ((j=1; j<=$numjobs; j++))
do
    echo "if [[ \$SLURM_ARRAY_TASK_ID == $j ]]; then" >> /tmp/$scriptfile
    #job=$cluster_path/$jobid/$(cat $jobsfile | nth $j)
    job=$(cat $jobsfile | nth $j) 
    echo -e "\t$job" >> /tmp/$scriptfile
    echo "fi" >> /tmp/$scriptfile
    echo "Writing job $j of $numbjobs to script file"
done

# Copy the directory to the cluster and launch the job
echo "Copying the directory to the cluster"
scp -r /tmp/$jobid $username@$cluster:$cluster_path
ssh -T $username@$cluster <<EOF 
    bash
    #opam update
    #opam upgrade
    # cd eclib
    # git pull
    # make
    # make install
    cd $cluster_path/$jobid 
    sbatch --array=1-$numjobs script.sh
    watch squeue
EOF
ssh -t $username@$cluster watch squeue
#ssh lehrepk@bluebear.bham.ac.uk "echo $jobid ; echo 1-$numjobs script.sh ; sleep 5"








